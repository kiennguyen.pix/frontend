# Base image
FROM node:14-alpine

# Set working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy project files
COPY . .

# Build the React app
RUN npm run build

# Expose port (change it if necessary)
EXPOSE 3000

# Command to start the app
CMD [ "npm", "start" ]
